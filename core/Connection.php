<?php
	use Baranda\Config;

	class Connection {
		private $driver, $host, $database, $username, $password, $charset, $collation;

		public function __construct() {
			$dbss = Config::getConfig()->database;
			$this->driver = $dbss['driver'];
			$this->host = $dbss['host'];
			$this->database = $dbss['database'];
			$this->username = $dbss['username'];
			$this->password = $dbss['password'];
			$this->charset = $dbss['charset'];
			$this->collation = $dbss['collation'];
		}

		public function connection() {
			if($this->driver == 'mysqli'){
				$connect = new mysqli($this->host, $this->username, $this->password, $this->database);
				$connect->query("SET NAMES '".$this->charset."'");
			}
			if ($connect->connect_errno) {
			    printf("Connect failed: %s\n", $connect->connect_error);
			    exit();
			}
			return $connect;
		}
	}
?>