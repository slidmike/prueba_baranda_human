<?php
	class User extends Model
	{
		private $table;

		public function __construct()
		{
			$table = 'user';
			parent::__construct($table);
		}

		public static function getUserByField($field,$value) {

		}

		public static function createNewUser($userData) {
			$entities = new BaseEntities('user');
			return $query = $entities->insert('user',[
				'user_name' => "'".$userData->name."'",
				'user_document' => $userData->document,
				'user_email' => "'".$userData->email."'",
				'user_country' => "'".$userData->country."'",
				'user_password' => "'".md5($userData->password)."'"
			]);
		}

		public static function SrcUserForLog($user) {
			$entities = new BaseEntities('user');
			$query = $entities->getLogin($user);

			if(count($query) > 0) {
				foreach ($query as $user) {
					$_SESSION['email'] = $user->user_email;
					$_SESSION['name'] = $user->user_name;
				}
			}

			return $query;
		}

		public static function findUsers($user) {
			$entities = new BaseEntities('user');
			$method = ($user->src == 'user_document') ? '=' : 'LIKE';
			return $entities->getBy($user->src,$method,$user->user);
		}
	}
?>