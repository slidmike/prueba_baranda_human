<?php
	class BaseEntities {
		private $table, $db, $connect;

		public function __construct($table) {
			$this->table = (string) $table;

			$this->connect = new Connection();
			$this->db = $this->connect->connection();
			$this->results = array();
		}

		public function getConnection() {
			return $this->connect;
		}

		public function db() {
			return $this->db;
		}

		public function getAll() {
			$query = $this->db->query("SELECT * FROM $this->table ORDER BY id DESC");

			while($row = $query->fetch_objects()){
				$this->results[] = $row;
			}

			return $this->results;
		}

		public function getBy($column,$method,$value){
			$value = is_string($value) ? "'%".$value."%'" : $value;

			$query = $this->db->query("SELECT * FROM $this->table WHERE $column Like $value");

			while($row = $query->fetch_object()){
				$this->results[] = $row;
			}

			return $this->results;
		}

		public function insert($tab,$values) {
			$k = array();
			$v = array();
			foreach ($values as $key => $value) {
				$k[] = $key;
				$v[] = $value;
			}
			$k = implode(',',$k);
			$v = implode(',',$v);

			$result = $this->db->query("INSERT INTO $this->table($k) VALUES ($v)");
			if ($result === false) {
				return $this->db->error;
			} else {
				return '1';
			}
		}

		public function getLogin($user) {
			$query = $this->db->query("SELECT * FROM $this->table WHERE user_email = '$user->email' AND user_password = '$user->password'");
			if ($query === false) {
				return $this->db->error;
			}
			while($row = $query->fetch_object()){
				$this->results[] = $row;
			}
			return $this->results;
		}
	}
?>
