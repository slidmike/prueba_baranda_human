<?php
	class Controller {
		public function __construct() {

		}

		public function view($view,$data=null) {
			if(is_array($data)){
				foreach ($data as $key => $value) {
					${$key} = $value;
				}
			}

			require_once(BASE_PATH.'core/ViewHelper.php');
			$helper = new ViewHelper();

			require_once BASE_PATH.'resources/views/'.$view.'.php';
		}
	}
?>