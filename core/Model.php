<?php
	class Model extends BaseEntities {
		private $table;

		public function __construct($table) {
			$this->table = (string) $table;
			parent::__construct($table);
		}

		public function executeSQL($query) {
			$theQuery = $this->db()->query($query);
			if($theQuery) {
				return $theQuery;
			} else {
				return false;
			}
		}
	}
?>