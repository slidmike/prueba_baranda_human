<!DOCTYPE html>
<html>
<head>
	<meta name="main-url" content="<?php echo BURL()->get('/'); ?>">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('style.css'); ?>">

	<style type="text/css"></style>
	<title>Baranda Human</title>
</head>
<body>
	<main class="main-baranda container">
		<section class="section-baranda">
			<h2 class="text-center">Welcome, <?php echo $name; ?></h2>
			<b><a class="text-danger text-center d-table ml-auto mr-auto" href="<?php echo BURL()->get('/logout'); ?>">Logout</a></b>
			<form class="form-baranda mt-5" id="search-form">
				<div class="form-group">
					<label>Search For Users</label>
					<input class="form-control" type="text" name="user" id="user">
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-6">
							<label>
								<span>By Name</span>
								<input type="radio" name="user-src" value="user_name" checked="checked">
							</label>
						</div>
						<div class="col-6">
							<label>
								<span>By Document</span>
								<input type="radio" name="user-src" value="user_document">
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary d-block ml-auto mr-auto" name="register" id="register">Search</button>
				</div>
				<div class="form-group">
					<h5 class="text-center text-log">Log for subscription</h5>
				</div>
			</form>
		</section>
		<section class="section-baranda">
			<table class="table" id="table-content">
				<thead>
					<tr>
						<th>Name</th>
						<th>Document</th>
						<th>email</th>
						<th>Country</th>
					</tr>
				</thead>
				<tbody id="content-users">
					
				</tbody>
			</table>
		</section>
	</main>
	
	<script type="text/javascript" src="<?php echo BURL()->getScript('jquery.js'); ?>"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BURL()->getScript('script-user.js'); ?>"></script>
</body>
</html>