<?php 

	$app            = System\App::instance();
	$app->request   = System\Request::instance();
	$app->route     = System\Route::instance($app->request);

	$route          = $app->route;

	$route->get('/', 'CustomerData@index');
	$route->get('/register', 'CustomerData@register');
	$route->get('/login', 'CustomerData@login');

	$route->get('/dd', 'CustomerData@index');

	$route->post('/create-user', 'CustomerData@CreateUser');
	$route->post('/login-user', 'CustomerData@LoginUser');
	$route->post('/src-users','CustomerData@searchUsers');

	$route->get('/logout','CustomerData@logout');



	$route->end();
	
?>