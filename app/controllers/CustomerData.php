<?php
	use Baranda\Session;
	class CustomerData extends Controller {

		public function __construct() {
			//$this->users = new User();
		}

		public function index() {
			$Middleware = new Middleware();
    		$Middleware->checkLogin();
			return Controller::view('index',array('name' => Session::get('name')));
		}

		public function register() {
			$Middleware = new Middleware();
    		$Middleware->checkLogout();

			return Controller::view('register');
		}

		public function CreateUser() {
			$userCreation = new stdClass();
			$userCreation->name = $_POST['name'];
			$userCreation->document = $_POST['document'];
			$userCreation->email = $_POST['email'];
			$userCreation->country = $_POST['country'];
			$userCreation->password = $_POST['password'];

			$query = User::createNewUser($userCreation);

			$log = new stdClass();
			$log->report = '0';
			$log->message = 'Unexpected Error';

			if($query == '1') {
				$log->report = '1';
				$log->message = 'User was added successfully, Redirecting...';
			} else {
				$log->message = $query;
			}

			echo json_encode($log);
		}

		public function login() {
			$Middleware = new Middleware();
    		$Middleware->checkLogout();

			return Controller::view('login');
		}

		public function LoginUser() {
			$userLogin = new stdClass();
			$userLogin->email = $_POST['email'];
			$userLogin->password = md5($_POST['password']);

			$query = User::SrcUserForLog($userLogin);

			$log = new stdClass();
			$log->report = '0';
			$log->message = 'User and password doesn\'t match!';

			if(count($query) > 0) {
				$log->report = '1';
				$log->message = 'Logged In, Redirecting...';
			}

			echo json_encode($log);
		}

		public function searchUsers() {
			$Middleware = new Middleware();
    		$Middleware->checkLogin();

			$userLogin = new stdClass();
			$userLogin->user = $_POST['user'];
			$userLogin->src = $_POST['src'];

			$query = User::findUsers($userLogin);

			echo json_encode($query);
		}

		public function logout() {
			Session::logout();
			BURL()->redirect('/');
		}
	}


?>