<!DOCTYPE html>
<html>
<head>
	<meta name="main-url" content="<?php echo BURL()->get('/'); ?>">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('style.css'); ?>">

	<style type="text/css"></style>
	<title>Baranda Human</title>
</head>
<body>
	<main class="main-baranda container">
		<section class="section-baranda">
			<form class="form-baranda" id="login-form">
				<h2 class="text-center">Log In</h2>
				<div class="form-group">
					<label>email</label>
					<input class="form-control" type="text" name="email" id="email" required="required">
				</div>
				<div class="form-group">
					<label>Password</label>
					<input class="form-control" type="password" name="password" id="password" required="required">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block" name="register" id="register">Log In</button>
				</div>
				<div class="form-group">
					<h5 class="text-center text-log">Log for subscription</h5>
				</div>
				<div class="form-group">
					<h5 class="text-center">Don't have an account? <a href="<?php echo BURL()->get('/register'); ?>">Register</a></h5>
				</div>
			</form>
		</section>
	</main>
	
	<script type="text/javascript" src="<?php echo BURL()->getScript('jquery.js'); ?>"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BURL()->getScript('script.js'); ?>"></script>
</body>
</html>