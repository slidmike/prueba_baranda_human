<?php
	require_once(BASE_PATH.'core/Config.php');
	require_once(BASE_PATH.'core/Connection.php');
	require_once(BASE_PATH.'core/URL.php');
	require_once(BASE_PATH.'core/Session.php');
	require_once(BASE_PATH.'core/Middleware.php');
	require_once(BASE_PATH.'core/BaseEntities.php');
	require_once(BASE_PATH.'core/Model.php');
	require_once(BASE_PATH.'core/Controller.php');

	foreach (glob(BASE_PATH.'app/models/*.php') as $file) {
		require_once($file);
	}
	foreach (glob(BASE_PATH.'app/controllers/*.php') as $file) {
		require_once($file);
	}

	//Show errors
	//===================================
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	//===================================

	require_once(BASE_PATH.'routes/web.php');
?>