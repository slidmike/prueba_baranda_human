<?php
	namespace Baranda;
	class Session {
		
		function __construct() {
			
		}

		public static function put($key,$val) {
			$_SESSION[$key] = $val;
		}

		public static function get($var) {
			if(isset($_SESSION[$var])){
				return $_SESSION[$var];
			} else {
				return null;
			}
		}

		public static function logout() {
			session_destroy();
		}
	}

?>