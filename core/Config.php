<?php
	namespace Baranda;
	
	Class Config {
		public function __construct() {
			$this->getConfig = new \stdClass();
		}

		public static function getConfig() {
			$getConfig = new \stdClass();
			$dir = scandir('config');
			foreach ($dir as $file) {
				$path = 'config/'.$file;
				if(is_file($path)){
					$getConfig->{pathinfo($path)['filename']} = require($path);
				}
			}
			return $getConfig;
		}
	}

?>