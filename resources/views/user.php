<!DOCTYPE html>
<html>
<head>
	<meta name="main-url" content="<?php echo BURL()->get('/'); ?>">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('fontawesome/css/all.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BURL()->getStyle('style.css'); ?>">

	<style type="text/css"></style>
	<title>Baranda Human</title>
</head>
<body>
	<main class="main-baranda container">
		<section class="section-baranda">
			<form class="form-baranda" id="register-form">
				<h2 class="text-center">Register</h2>
				<div class="form-group">
					<label>Name</label>
					<input class="form-control" type="text" name="nombre" id="nombre" required="required">
				</div>
				<div class="form-group">
					<label>Document</label>
					<input class="form-control" type="text" name="document" id="document" required="required">
				</div>
				<div class="form-group">
					<label>email</label>
					<input class="form-control" type="text" name="email" id="email" required="required">
				</div>
				<div class="form-group">
					<label>Country</label>
					<select class="form-control" type="text" name="country" id="country"></select>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input class="form-control" type="password" name="password" id="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block" name="register" id="register">Register</button>
				</div>
				<div class="form-group">
					<h5 class="text-center text-log">Log for subscription</h5>
				</div>
				<div class="form-group">
					<h5 class="text-center">Already have an account? <a href="<?php echo BURL()->get('/login'); ?>">Log in</a></h5>
				</div>
			</form>
		</section>
	</main>
	
	<script type="text/javascript" src="<?php echo BURL()->getScript('jquery.js'); ?>"></script>
	<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo BURL()->getScript('countries.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo BURL()->getScript('script.js'); ?>"></script>
</body>
</html>