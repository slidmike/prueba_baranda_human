<?php 
	use Baranda\Config;

	class URL {
		public function __construct() {
			$this->currentPath = $_SERVER['PHP_SELF']; 
		    $this->pathInfo = pathinfo($this->currentPath); 
		    $this->hostName = $_SERVER['HTTP_HOST']; 
		    $this->protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
		    $this->mainURL = $this->protocol.$this->hostName.$this->pathInfo['dirname'];
		    $this->styleURL = $this->mainURL.'/resources/assets/css/';
		    $this->scriptURL = $this->mainURL.'/resources/assets/js/';
		}

		public function redirect($url=null) {
			$TheUrl = ($url === null || $url === '') ? '/' : $url;
			header('Location: '.$this->mainURL.$TheUrl);
		}

		public function get($url=null) {
			return $this->mainURL.$url;
		}

		public function getStyle($style=null) {
			return $this->styleURL.$style;
		}

		public function getScript($script=null) {
			return $this->scriptURL.$script;
		}
	}

	function BURL() {
		return new URL();
	}
?>