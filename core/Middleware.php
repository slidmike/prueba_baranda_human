<?php
	use Baranda\Session;
	class Middleware
	{
		public function __construct() {
			
		}

		public function checkLogin() {
			if(Session::get('email') === null){ 
				BURL()->redirect('/login');
			}
		}

		public function checkLogout() {
			if(Session::get('email') !== null){ 
				BURL()->redirect('/');
			}
		}
	}
?>