$(function(e){

	$.fn.disableButton = function(replace) {
		$(this).attr('data-text', $(this).html() );
		$(this).html(replace);
		$(this).attr('disabled','disabled');
	}
	$.fn.enableButton = function() {
		if( $(this).attr('data-text') != undefined ) { $(this).html( $(this).attr('data-text') ) };
		$(this).removeAttr('disabled');
	}

	//utilities
	var loadingIcon = '<i class="fas fa-spinner fa-pulse"></i>';
	var mainURL = $('meta[name="main-url"]').attr('content');
	
	//DOM
	var registerForm = $('#register-form');
	var LoginForm = $('#login-form');

	function attempRegisterUser(form) {
		var thisForm = $(form);
		var btnSubmit = $(form).find('button[type="submit"]');
		$(btnSubmit).disableButton(loadingIcon);

		var objectPost = {
			name : $(form).find('input[name="nombre"]').val(),
			document : $(form).find('input[name="document"]').val(),
			email : $(form).find('input[name="email"]').val(),
			country : $(form).find('select[name="country"]').val(),
			password : $(form).find('input[name="password"]').val()
		}

		$.ajax({
			url : mainURL+'create-user',
			type : 'POST',
			data : objectPost
		}).done(function(res){
			JsonRes = JSON.parse(res);

			$(btnSubmit).enableButton();
			if(JsonRes.report == '1'){
				$(thisForm).find('.text-log').addClass('text-success');
				$(thisForm).find('.text-log').removeClass('text-danger');
				setTimeout(function(){ window.location = mainURL+'login'; }, 100);
			} else {
				$(thisForm).find('.text-log').addClass('text-danger');
				$(thisForm).find('.text-log').removeClass('text-success');
			}
			$(thisForm).find('.text-log').text(JsonRes.message);
			$(thisForm).find('.text-log').addClass('show');
		}).fail(function(res){
			$(btnSubmit).enableButton();
			console.log(res);
		});
	}

	function attempLoginUser(form) {
		var thisForm = $(form);
		var btnSubmit = $(form).find('button[type="submit"]');
		$(btnSubmit).disableButton(loadingIcon);

		var objectPost = {
			email : $(form).find('input[name="email"]').val(),
			password : $(form).find('input[name="password"]').val()
		}

		$.ajax({
			url : mainURL+'login-user',
			type : 'POST',
			data : objectPost
		}).done(function(res){
			var JsonRes = JSON.parse(res);

			if(JsonRes.report == '1'){
				$(thisForm).find('.text-log').addClass('text-success');
				$(thisForm).find('.text-log').removeClass('text-danger');
				setTimeout(function(){ window.location = mainURL; }, 100);
			} else {
				$(thisForm).find('.text-log').addClass('text-danger');
				$(thisForm).find('.text-log').removeClass('text-success');
				$(btnSubmit).enableButton();
			}
			$(thisForm).find('.text-log').text(JsonRes.message);
			$(thisForm).find('.text-log').addClass('show');

		}).fail(function(res){
			$(btnSubmit).enableButton();
			console.log(res);
		});
	}

	$(registerForm).on('submit', function(event){ event.preventDefault(); attempRegisterUser(this); });
	$(LoginForm).on('submit', function(event){ event.preventDefault(); attempLoginUser(this); });

});