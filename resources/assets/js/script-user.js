$(function(e){

	$.fn.disableButton = function(replace) {
		$(this).attr('data-text', $(this).html() );
		$(this).html(replace);
		$(this).attr('disabled','disabled');
	}
	$.fn.enableButton = function() {
		if( $(this).attr('data-text') != undefined ) { $(this).html( $(this).attr('data-text') ) };
		$(this).removeAttr('disabled');
	}

	//utilities
	var loadingIcon = '<i class="fas fa-spinner fa-pulse"></i>';
	var mainURL = $('meta[name="main-url"]').attr('content');
	
	//DOM
	var SearchForm = $('#search-form');
	var contentUser = $('#content-users');

	function searchUsers(form) {
		var thisForm = $(form);
		var btnSubmit = $(form).find('button[type="submit"]');
		$(btnSubmit).disableButton(loadingIcon);
		$(thisForm).removeClass('show')

		var objectPost = {
			user : $(form).find('input[name="user"]').val(),
			src : $(form).find('input[name="user-src"]:checked').val()
		}

		$.ajax({
			url : mainURL+'src-users',
			type : 'POST',
			data : objectPost
		}).done(function(res){
			JsonRes = JSON.parse(res);
			$(btnSubmit).enableButton();

			if(JsonRes.length > 0) {
				$(thisForm).find('.text-log').text('The search has '+JsonRes.length+' results');
				$(thisForm).find('.text-log').addClass('show');

				for(i=0;i<JsonRes.length;i++){
					$(contentUser).append('<tr>\
						<td>'+JsonRes[i].user_name+'</td>\
						<td>'+JsonRes[i].user_document+'</td>\
						<td>'+JsonRes[i].user_email+'</td>\
						<td>'+JsonRes[i].user_country+'</td>\
					</tr>');
				}
			} else {
				$(thisForm).find('.text-log').text('The search has 0 results');
				$(thisForm).find('.text-log').addClass('show');
			}
		}).fail(function(res){
			$(btnSubmit).enableButton();
			console.log(res);
		});

		$(contentUser).empty();
	}

	$(SearchForm).on('submit', function(event){ event.preventDefault(); searchUsers(this); });
});